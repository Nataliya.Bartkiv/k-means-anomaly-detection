import pickle
import json
import codecs
import numpy as np

class Result:
	def __init__(self, value, error):
		self.value = value
		self.error = error

class ResultEncoder(json.JSONEncoder):
	def default(self, o):
		return o.__dict__  


def sliding_chunker(data, window_len, slide_len):
	"""
	Split a list into a series of sub-lists, each sub-list window_len long,
	sliding along by slide_len each time. If the list doesn't have enough
	elements for the final sub-list to be window_len long, the remaining data
	will be dropped.

	e.g. sliding_chunker(range(6), window_len=3, slide_len=2)
	gives [ [0, 1, 2], [2, 3, 4] ]
	"""
	chunks = []

	for pos in range(0, len(data), slide_len):
		chunk = np.copy(data[pos:pos+window_len])
		if len(chunk) != window_len:
			continue
		chunks.append(chunk)

	return chunks


def reconstruct(data, window, clusterer):
	"""
	Reconstruct the given data using the cluster centers from the given
	clusterer.
	"""
	window_len = len(window)
	slide_len = int(window_len/2)
	segments = sliding_chunker(data, window_len, slide_len)
	reconstructed_data = np.zeros(len(data))
	for segment_n, segment in enumerate(segments):
		# window the segment so that we can find it in our clusters which were
		# formed from windowed data
		segment *= window
		segment2D = segment.reshape(1, -1)

		nearest_match_idx = clusterer.predict(segment2D)[0]
		nearest_match = np.copy(clusterer.cluster_centers_[nearest_match_idx])

		pos = segment_n * slide_len
		reconstructed_data[pos:pos+window_len] += nearest_match

	return reconstructed_data


def predict(model_path, json_string):
	segment_len = 8

	window_rads = np.linspace(0, np.pi, segment_len)
	window = np.sin(window_rads)**2

	clusterer = pickle.load(open(model_path, "rb"))
	
	# json_string = codecs.open(dataset_path, 'r', encoding='utf-8').read()
	# test_list = json.loads(json_string)
	# test_dataset = np.array(test_list)
	test_dataset = np.array(json_string)

	reconstruction = reconstruct(test_dataset, window, clusterer)
    
	error = reconstruction - test_dataset

	result_list = list()

	for index, value in enumerate(test_dataset):
		result = Result(value, error[index])
		result_list.append(result)

	result_json = ResultEncoder().encode(result_list)
	return result_json


if __name__ == "__main__":
	model_path = "model.pkl"
	dataset_path = "test_dataset.json"

	json_string = codecs.open(dataset_path, 'r', encoding='utf-8').read()

	predict(model_path, json_string)